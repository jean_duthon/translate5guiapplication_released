/**
 * Created by jean on 5/30/2016.
 */


Ext.define('Editor.RESTStateProvider', {
    extend : 'Ext.state.Provider',
    alias : 'state.jsonpstorage',

    config: {
        url: Editor.data.restpath + "stateprovider",
        timeout: 30000
    },

    constructor : function(config) {
        this.initConfig(config);
        var me = this;

        me.restoreState();
        me.callParent(arguments);
    },
    set : function(name, value) {
        var me = this;

        if( typeof value == "undefined" || value === null) {
            me.clear(name);
            return;
        }
        me.persist(name, value);
        me.callParent(arguments);
    },
    restoreState : function(callback) {
        var me = this;
        me.isLoadingState = true;
        if(Editor.data.task && Editor.data.task.taskGuid){
            me.currentTaskGuid = Editor.data.task.taskGuid;
        }
        Ext.Ajax.request({
            url : this.getUrl(),
            method: 'get',
            success : function(results) {
                var json = Ext.decode(results.responseText);
                results = json.rows;
                //First remove all states since state is based on taskGuid
                for(var i in me.state){
                    delete me.state[i];
                }
                for(var i in results) {
                    me.state[i] = me.decodeValue(results[i]);
                }

                if(callback){
                    callback();
                }
            },
            failure : function() {
                console.log('failed', arguments);
            },
            scope: this
        });
    },  
    // private
    clear : function(name) {
        this.clearKey(name);
        this.callParent(arguments);
    },
    // private
    persist : function(name, value) {
        var me = this;

        Ext.Ajax.request({
            url : this.getUrl(),
            method: 'put',
            params: {name : name, value: me.encodeValue(value)},
            success : function(results) {
                var json = Ext.decode(results.responseText);
                var currentObj;
                results = json.row;
                for(var i in results) {
                    me.state[i] = me.decodeValue(results[i]);
                }
            },
            failure : function() {
                console.log('failed', arguments);
            },
            scope: me
        });
    },
    clearKey : function(name) {
        Ext.data.JsonP.request({
            url : this.getUrl(),
            params : {
                method : 'clear',
                name : name
            },
            disableCaching : true,
            success : function() {
                console.log('success');
            },
            failure : function() {
                console.log('failed', arguments);
            }
        });
    },
    forceStateRestore: function(callback){
        var me = this;
        me.restoreState(callback);
    }
});