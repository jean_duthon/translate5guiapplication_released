
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**#@++
 * @author Marc Mittag
 * @package editor
 * @version 1.0
 *
 */
/**
 * Controller für den Wiederholungseditor
 * @class Editor.controller.ChangeAlike
 * @extends Ext.app.Controller
 * 
 * IMPORTANT:
 * ChangeAlikes are working only with a default column layout 
 * of source and target column (optional relais)! 
 * ChangeAlikes does not work with Alternates!!!
 * 
 * Binding to the save chain: 
 * Method onAfterSaveCall to save chain event "afterSaveCall"
 * Method onSaveComplete to save chain event "saveComplete"
 * Method handleBeforeEdit loads the change alikes of a segment and fires event "fetchChangeAlikes" with the running AJAX operation as parameter
 *   The SegmentController is bound to "fetchChangeAlikes" and injects a callback with the next step in the save chain if the operation is still running.
 * 
 * handleAlikesRead is triggered after reading the change alikes, and steps back to the saveChain if needed.
 * 
 * saveChain Flow:
 * in controller.Segment                                               in controller.ChangeAlike
 * saveChainStart
 *      ↓
 * saveChainCheckAlikes         → if still loading change alikes:
 *      ↓ directly if alikes      set callback to operation, 
 *      ↓ are already loaded      which is triggered in                handleAlikesRead
 * saveChainSave 
 *      ↓
 * saveChainSaveCallback 
 *      ↓
 * saveChainEnd
 * 
 */
Ext.define('Editor.controller.ChangeAlike', {
  extend : 'Ext.app.Controller',
  stores: ['AlikeSegments','Segments'],
  views: ['changealike.Window'],
  messages: {
    alikeSingular: '#UT#Wiederholung wurde bearbeitet und gespeichert',
    alikePlural: '#UT#Wiederholungen wurden bearbeitet und gespeichert',
    alikesDisabled: '#UT#Das Projekt enthält alternative Übersetzungen. Der Wiederholungseditor wurde daher deaktiviert.'
  },
  id: 'changealikecontroller',
  alikesToProcess: null,
  fetchedAlikes: null,
  saveIsRunning: false,
  window: null,
  alikeSegmentsUrl: '',
  actualRecord: null,
  timeTracking: null,
  isDisabled: false,
  callbackToSaveChain: Ext.emptyFn,
  refs : [{
    ref : 'segmentGrid',
    selector : '#segmentgrid'
  },{
    ref : 'alikeWindow',
    selector : '#changealikeWindow'
  },{
    ref : 'alikeGrid',
    selector : '#changealikeWindow gridpanel'
  },{
    ref : 'optionsBtn',
    selector : '#segmentgrid #optionsBtn'
  }],
  listen: {
      component: {
          '#changealikeWindow #saveBtn' : {
              click: 'handleSaveChangeAlike'
          },
          '#changealikeWindow' : {
              show: 'focusButton',
              onEscape: 'handleCancelChangeAlike'
          },
          '#changealikeWindow #cancelBtn' : {
              click: 'handleCancelChangeAlike'
          },
          '#changealikeWindow tool[type=close]' : {
              click: 'handleCancelChangeAlike'
          },
          '#segmentgrid': {
              afterrender: 'initEditPluginHandler'
          }
      },
      controller: {
          '#Editor.$application': {
              editorViewportClosed: 'clearAlikeSegments'
          },
          '#segmentscontroller': {
              afterSaveCall: 'onAfterSaveCall',
              saveComplete: 'onSaveComplete'
          }
      }
  },
  /**
   * Gibt die RowEditing Instanz des Grids zurück
   * @return {Editor.view.segments.RowEditing}
   */
  getEditPlugin: function() {
    return this.getSegmentGrid().editingPlugin;
  },
  /**
   * initiert Handler und Daten die erst nach dem rendern des Viewports gesetzt werden können
   * @see {Ext.app.Controller}
   */
  onLaunch: function() {
    var me = this;
    //Diese Events können erst in onlauch gebunden werden, in init existiert das Plugin noch nicht
    me.alikeSegmentsUrl = me.getStore('AlikeSegments').getProxy().url;
    me.window = Ext.widget('changealikeWindow');
  },
  /**
   * inits the editing plugin
   */
  initEditPluginHandler: function() {
      var me = this,
          t = Editor.data.task,
          auth = Editor.app.authenticatedUser,
          enabledACL = auth.isAllowed('useChangeAlikes');
          enabled = auth.isAllowed('useChangeAlikes', t);
      //disable the whole settings button, since no other settings are currently available!
      me.getOptionsBtn().setVisible(enabled);
      me.isDisabled = ! enabled;
      me.getEditPlugin().on('beforeedit', me.handleBeforeEdit, me);
      if(!t.get('defaultSegmentLayout') && enabledACL) {
          Editor.MessageBox.addInfo(this.messages.alikesDisabled, 1.4);
      }
  },
  focusButton: function(win) {
      win.down('#saveBtn').focus(false, 201);
  },
  clearAlikeSegments: function() {
      this.getAlikeSegmentsStore().removeAll();
  },
  /**
   * Handler to load the alike segments of actual segment.
   * Danger: if adding new methods to open segments, it must be assured, 
   * that this method is called not before the "Next Step callbacks" in handleSaveChangeAlike
   * fires event "fetchChangeAlikes" with the current AJAX operation as parameter.
   * 
   * @param {Ext.grid.plugin.Editing} column
   */
  handleBeforeEdit: function(editingPlugin, context) {
    var me = this, 
        rec = context.record,
        //id des bearbeiteten Segments
        id = rec.get('id'),
        store = me.getStore('AlikeSegments'),
        segmentStore = me.getSegmentGrid().getStore()
        segmentsProxy = segmentStore.getProxy(),
        params = {};
    
    if(me.isDisabled || me.isManualProcessingDisabled()) {
        return;
    }
    
    params[segmentsProxy.getFilterParam()] = segmentsProxy.encodeFilters(segmentStore.getFilters().items);
    params[segmentsProxy.getSortParam()] = segmentsProxy.encodeSorters(segmentStore.getSorters().items);
    
    store.load({
        url: me.alikeSegmentsUrl+'/'+id,
        params: params,
        //prevent default ServerException handling
        preventDefaultHandler: true,
        callback: function(recs, op, success) {
            success && me.handleAlikesRead(op, id);
        }
    });
  },

  /**
   * handle if alikes are successful read
   * @param {Ext.data.Operation} operation
   * @param {Integer} id
   */
  handleAlikesRead: function(operation, id) {
      var me = this 
      
      if(me.isDisabled || ! operation.wasSuccessful()){
          //@todo Meldung machen, dass keine WDHs geholt werden konnten!
          operation.handleReadAfterSave && operation.handleReadAfterSave();
          return;
      }
      
      me.fetchedAlikes = operation.getRecords();
      if(me.isManualProcessing()) {
          me.window.setAlikes(id, me.fetchedAlikes);
      }
      operation.handleReadAfterSave && operation.handleReadAfterSave();
  },
  
  /**
   * is invoked by the save chain, directly after starting the save request of the segment
   * @param {Function} finalCallback to return to save chain
   */
  onAfterSaveCall: function(finalCallback, record) {
      var me = this;
      me.callbackToSaveChain = finalCallback;
      me.actualRecord = record;
      me.saveIsRunning = true;
      if(me.isDisabled || me.isManualProcessingDisabled() || me.noAlikes()) {
          me.fireEvent('segmentUsageFinished', me);
          me.callbackToSaveChain();
          return;
      }
      if(me.isAutoProcessing()) {
          me.handleSaveChangeAlike();
          return;
      }
      //manualProcessing:
      me.timeTracking = new Date(); // starting the time tracking
      me.window.show(record);
  },
  /**
   * @return boolean true, if no alikes are present
   */
  noAlikes: function() {
      var me = this;
      me.allAlikes = me.getAllAlikeIds();
      if(Ext.isEmpty(me.allAlikes)) {
          return true;
      }
      return false;
  },
  /**
   * checks if source editing is enabled
   * @returns {Boolean}
   */
  getSourceEditing: function() {
      return Editor.data.task.get('enableSourceEditing');
  },
  /**
   * Startet das Speichern der Wiederholungen. Wird je nach Einstellung automatisch oder manuell getriggert.
   */
  handleSaveChangeAlike: function() {
    var me = this,
        rec = me.actualRecord;
    
    //Daten des aktuelle bearbeiteten Segments, die angezeigten AlikeSegmente im Segment Store werden mit diesen überschrieben 
    //Hier wird auch das Alike Segment vorübergehend auf nicht editierbar gesetzt, bis das OK vom Server kommt
    data = {
      stateId: rec.data.stateId,
      qmId: rec.data.qmId,
      editable: 0,
      autoStateId: 999
    };
    if(me.getSourceEditing()) {
        data.sourceEdit = rec.data.sourceEdit;
    }
    else {
        data.source = rec.data.source;
    }
    data.targetEdit = rec.data.targetEdit;
    me.alikesToProcess = me.getAlikesToProcess();
    me.calculateUsedTime();
    
    //Next Step in save chain Callback
    me.fireEvent('segmentUsageFinished', me);
    //die 
    //alike segmente mit den Änderungen befüllen, aber noch nicht comitten, erst im alikesSaveSuccessHandler wenn die Alike Segmente auf dem Server gespeichert sind
    Ext.Array.each(me.alikesToProcess, function(alikeId){
        me.updateSegment(alikeId, data);
    });
    
    //ab hier nur bei manuellem processing der Alike Segmente
    if(me.isManualProcessing()) {
        me.window.close();
    }
    
    if(!me.saveIsRunning) {
        me.savePendingAlikes();
    }
  },
  /**
   * calculates the elapsed time the user needs to process the change alikes 
   */
  calculateUsedTime: function() {
      var me = this;
      //if it was a date, this was the opening time of the change alike editor
      if(me.timeTracking instanceof Date) {
          me.timeTracking = (new Date()) - me.timeTracking;
      }
      else {
          //otherwise it is automatic handling, so that we assume 0 as time.
          me.timeTracking = 0;
      }
  },
  /**
   * Handler is called after saving a segment successfully to the server in save chain (called by Ajax callback)
   * @return {Boolean}
   */
  onSaveComplete: function(){
      var me = this;
      if(me.alikesToProcess) { 
          me.savePendingAlikes();
      }
      me.saveIsRunning = false;
      //if no alikes are used, return to save chain
      if(!me.isManualProcessingDisabled()) {
          //Sending an event so the finalCallback is called afterwards to proceed
          me.fireEvent("saveChainEnd");
      }
  },
  /**
   * Stößt auf der Server Seite die Verarbeitung Wiederholungen an.  
   * @param {Number[]} alikes Array mit den zu bearbeitenden Segment IDs
   */
  savePendingAlikes: function() {
    var me = this,
        id = me.actualRecord.getId(),
        alikes = me.alikesToProcess;
    if(!alikes || alikes.length == 0) {
        me.callbackToSaveChain();
        return;
    }
    Ext.Ajax.request({
      toSegmentId: id,
      url: me.alikeSegmentsUrl+'/'+id,
      method: 'put',
      params: {
          "duration": me.timeTracking,
          "alikes[]": alikes
      },
      success: me.alikesSaveSuccessHandler,
      failure: me.alikesSaveFailureHandler,
      scope: me
    });
  },
  /**
   * gibt ein Array mit den als Wiederholungen zu bearbeitenden SegmentIDs zurück
   * @return {Number[]}
   */
  getAlikesToProcess: function() {
      var me = this, 
          allIds = me.getAllAlikeIds();
      if(me.isAutoProcessing()){
          return allIds;
      }
      return me.getSelectedAlikeIds(allIds);
  },
  /**
   * returns an array with all alike ids
   * @returns {Array}
   */
  getAllAlikeIds: function() {
    var result = [];
    Ext.Array.each(this.fetchedAlikes, function(rec){
        result.push(rec.get('id'));
    });
    return result;
  },
  /**
   * Gibt ein Array mit den SegmentIDs anhand der Auswahl im Wiederholungseditor an
   * @param {Number[]} allIds an array with all valid change alike ids for this call, 
   * @return {Number[]}
   */
  getSelectedAlikeIds: function(validIds) {
      var sel = this.getAlikeGrid().getSelectionModel().getSelection(),
          selIds = this.getIdsFromRecords(sel);
      return Ext.Array.intersect(validIds, selIds);
  },
  /**
   * Helper: mappt die id's eines record Arrays in ein Array mit IDs
   * @param {Editor.model.AlikeSegment[]} recordArray
   * @return {Number[]}
   */
  getIdsFromRecords: function(recordArray) {
    return Ext.Array.map(recordArray, function(record){
      return record.get('id');
    });
  },
  /**
   * Fehlerhandler wenn AlikeSegmente nicht gespeichert werden konnten
   * @param {Object} resp
   * @param {Object} options
   */
  alikesSaveFailureHandler: function(resp, options) {
    this.cleanUpAlikeSegments();
    Editor.MessageBox.addError(this.messages.alikesFailure);
  },
  /**
   * Handler für erfolgreiches Speichern von Alike Segmenten
   * @param {Object} resp
   * @param {Object} options
   */
  alikesSaveSuccessHandler: function(resp, options) {
    var me = this,
    //id des Ziel Segments
    data = Ext.decode(resp.responseText),
    alikes = me.alikesToProcess,
    alikesSaved = (data.rows.length == 1 ? me.messages.alikeSingular : me.messages.alikePlural);
    if(!data.rows || data.rows.length == 0) {
 	    me.cleanUpAlikeSegments();
    	return;
    }
    //Auslesen und Verarbeiten der IDs der Alike Segmente die auf dem Server erfolgreich gespeichert wurden
    Ext.Array.each(data.rows, function(item) {
    	var updateId = parseInt(item.id);
    	//Erfolgreich als Wdh. bearbeitete und gespeicherte Segmente im Segment Store comitten:
    	var rec = me.updateSegment(updateId, item);
    	//der eigene Cache im Record wird hier (im PUT Alikes Success) nicht mehr benötigt:
    	rec && delete rec._editorDataSave;
    	//Die ID aus dem pendingArray entfernen
    	if(alikes && Ext.isArray(alikes)){
    	    Ext.Array.remove(alikes, updateId);
    	}
    });
    
    me.cleanUpAlikeSegments();
    Editor.MessageBox.addSuccess(Ext.String.format(alikesSaved, data.rows.length));
  },
  /**
   * Die übriggebliebenen IDs in der Pending Liste wurden auf dem Server nicht erfolgreich gespeichert, 
   * und werden daher im Segments Store rejectet, sprich wieder die unveränderten Daten angezeigt 
   * 
   * This method should be the last called method in the changealike processing. Its responsible to jump back to the saveChain 
   */
  cleanUpAlikeSegments: function() {
      var me = this,
          alikes = me.alikesToProcess;
      if(!alikes) {
          me.callbackToSaveChain();
          return;
      }
      Ext.Array.each(alikes, function(revertId){
          var rec = me.getStore('Segments').getById(revertId);
          if(rec) {
              rec.data = Ext.apply({}, rec._editorDataSave);
              rec.reject();
              delete rec._editorDataSave;
          }
      });
      me.alikesToProcess = null;
      me.callbackToSaveChain();
  },
  /**
   * Befüllt das Segment mit der gegebenen ID im Segment Store mit den übergebenen Daten,
   * ohne den Segment Store über die Änderung zu informieren (dass kein automatisches PUT stattfindet)
   * speichert die Orginal Daten für ein Roleback des Segments. Die vorhandene reject / commit Methodik kann 
   * zur Zwischenspeicherung hier nicht verwendet werden, da diese sonst automatisch den Store gegen den Server synct. 
   * @param {Number} id
   * @param {Object} data
   * @return {Editor.model.Segment}
   */
  updateSegment: function(id, data) {
    var store = this.getStore('Segments'),
        rec = store.getById(id);
    if(!rec) {
      if(!store.prefetchData){
          return null;
      }
      rec = store.prefetchData.findBy(function(rec){
          return rec.internalId == id;
      });
      if(!rec) {
          return null;
      }
    }
    rec._editorDataSave = Ext.apply({}, rec.data);
    rec.beginEdit();
    rec.set(data);
    rec.endEdit(true);
    rec.commit();
    return rec;
  },
  handleCancelChangeAlike: function() {
      this.callbackToSaveChain();
      this.window.close();
      return false; //prevent default close action
  },
  isManualProcessingDisabled: function() {
    return (Editor.data.preferences.alikeBehaviour == 'never');
  }, 
  isAutoProcessing: function() {
    return (Editor.data.preferences.alikeBehaviour == 'always');
  }, 
  isManualProcessing: function() {
    return (Editor.data.preferences.alikeBehaviour == 'individual');
  }
});