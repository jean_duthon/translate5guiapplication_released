
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**#@++
 * @author Marc Mittag
 * @package editor
 * @version 1.0
 *
 */
/**
 * Encapsulates all logic for qmsubsegment- and qmsummary-feature
 * @class Editor.controller.QmSubSegments
 * @extends Ext.app.Controller
 */
Ext.define('Editor.controller.QmSubSegments', {
    extend : 'Ext.app.Controller',
    views: ['qmsubsegments.Window', 'ToolTip'],
    requires: ['Editor.store.QmSummary'], //QmSummary Store in "requires" instead "stores" to prevent automatic instantiation
    refs:[{
        ref : 'window',
        selector : '#qmsummaryWindow',
        autoCreate: true,
        xtype: 'qmSummaryWindow'
    },
    {
    	ref : 'addMenuFirstLevel',
    	selector : 'qmSubsegmentsFlagFieldset button > menu' //first menu level
    },
    {
    	ref : 'qmFieldset',
    	selector : 'qmSubsegmentsFlagFieldset' //first menu level
    },{
	    ref : 'segmentGrid',
	    selector : '#segmentgrid'
    },{
	    ref : 'metaInfoForm',
	    selector : '#metaInfoForm'
    },{
        ref: 'metaSevCombo',
        selector: '#metapanel combobox[name="qmsubseverity"]'
    },{
            ref: 'QMAddBtn',
            selector: '#QMAddBtn'
        }],
    selectedQmSegment: null,
    /**
     * Deactivated this feature for IE, see EXT6UPD-111
     */
    constructor: function() {
        this.disableIE();
        this.callParent(arguments);
    },
    disableIE: function() {
        if(!Ext.isIE) {
            return;
        }
        var msg = function() {
                Editor.MessageBox.addInfo('MQM Issues can not be used with this Version of Internet Explorer!');
            };
        this.config.listen = {
            component: {
                '#segmentgrid': {
                    afterrender: msg
                },
                '#segmentgrid #qmsummaryBtn': {
                    click: msg
                }
            }
        };
    },
    listen: {
        controller: {
            '#editorcontroller': {
                'assignMQMTag': 'handleAddQmFlagKey'
            }
        },
        component: {
            '#segmentgrid #qmsummaryBtn': {
                click:'showQmSummary'
            },
            '#segmentgrid': {
                afterrender: 'handleAfterRender'
            },
            'qmSubsegmentsFlagFieldset menuitem': {
                click: 'handleQmFlagClick'
            },
            '#QMSaveBtn': {
                click: 'saveCurrentMQM'
            },
            '#metaInfoForm': {
                afterrender: 'handleInitEditor'
            },
            'segmentsHtmleditor': {
                afterinitframedoc: 'initIframeDoc',
                initialize: 'initEditor'
            }
        }
    },
    strings: {
    	buttonTooltip10: '#UT# (ALT+{0})',
    	buttonTooltip20: '#UT# (ALT+SHIFT+{0})'
    },
    handleAfterRender: function(){
        var me = this;
        
        me.tooltip = Ext.create('Editor.view.ToolTip', {
            target: me.getSegmentGrid().getEl()
        });
    },
    handleInitEditor: function() {
        this.initFieldSet();
        var combo = this.getMetaSevCombo(),
            sevStore = Ext.create('Ext.data.Store', {
                fields: ['id', 'text'],
                storeId: 'Severities',
                data: Editor.data.task.get('qmSubSeverities')
            });
        //bindStore dynamically to combo:
        combo.bindStore(sevStore);
        combo.setValue(sevStore.getAt(0).get('id'));
        combo.resetOriginalValue();
    },
    /**
     * initialises the QM SubSegment Fieldset in the MetaPanel
     */
    initFieldSet: function() {
    	if(!Editor.data.task.hasQmSub()){
    		return;
    	}
    	var mpForm = this.getMetaInfoForm(),
    		pos = mpForm.items.findIndex('itemId', 'metaQm');
    	mpForm.insert(pos, {xtype: 'qmSubsegmentsFlagFieldset', menuConfig: this.getMenuConfig()});
    },
    /**
     * generates the config menu tree for QM Flag Menu
     * @returns
     */
    getMenuConfig: function() {
		Editor.qmFlagTypeCache = {};
		var me = this,
			cache = Editor.qmFlagTypeCache,
		iterate = function(node) {
			var result, 
			    text, id;
			if(Ext.isArray(node)){
				result = [];
				Ext.each(node, function(item) {
					result.push(iterate(item));
				});
				return result;
			}
			
			text = node.text;
			if(node.id <= 10) {
			    id = node.id == 10 ? 0 : node.id;
			    text += Ext.String.format(me.strings.buttonTooltip10, id);
			}
			else if(node.id > 10 && node.id <= 20) {
			    text += Ext.String.format(me.strings.buttonTooltip20, ('' + node.id).substr(1));
			}
			
			result = {
				text: text,
				qmid: node.id,
				icon: me.getImgTagSrc(node.id, true),
				qmtype: 'qm-'+node.id,
				menuAlign: 'tr-tl'
			};
			cache[node.id] = node.text; 
			if(node.children && node.children.length > 0) {
				result.menu = {
					enableKeyNav: false,
					bodyCls: 'qmflag-menu',
					items: iterate(node.children),
	                listeners: {
	                    afterrender: function(component) {
	                    	if(component.keyNav) {
	                    		component.keyNav.disable();
	                    	}
	                    }
                	}
				};
			}
			return result;
		};
		return iterate(Editor.data.task.get('qmSubFlags'));
	},
    /**
     * initialises things related to the editors iframe 
     * @param editor
     */
    initIframeDoc: function(editor) {
        if(Ext.isIE){
            editor.iframeEl.on('beforedeactivate', this.handleEditorBlur, this);
            editor.iframeEl.on('focus', this.handleEditorFocus, this);
        }
    },
    /**
     * initialises ToolTips and other things related to the editors iframe doc body
     * @param editor
     */
    initEditor: function(editor) {
        var me = this;
        if(me.editorTooltip){
            me.editorTooltip.setTarget(editor.getEditorBody());
            me.editorTooltip.boundFrame = editor.iframeEl;
            return;
        }
        me.editorTooltip = Ext.create('Editor.view.ToolTip', {
            target: editor.getDoc(),
            boundFrame: editor.iframeEl
        });
        editor.getDoc().addEventListener("click", function(e){
            if(e.target.id.indexOf("qm-image-") != -1){
                me.selectQmSegment(e.target);
                var selection = editor.getDoc().getSelection();
                var range = editor.getDoc().createRange();
                range.setStartBefore(e.target.nextSibling);
                range.setEndBefore(e.target.nextSibling);
                selection.removeAllRanges();
                selection.addRange(range);
            } else {
                me.unselectQmSegment();
            }
        });
        me.setDOMNodeRemovedEvent(editor);
    },
    DOMObserver: null,
    setDOMNodeRemovedEvent: function(editor){
        var me = this;
        if(me.DOMObserver == null){
            me.DOMObserver = new MutationObserver(function(mutations){
                mutations.forEach(function(mutation) {
                    var removedCount, curRemoved;
                    if(mutation.type === "childList"){
                        removedCount = mutation.removedNodes.length;
                        for(var i=0; i<removedCount;++i){
                            curRemoved = mutation.removedNodes[i];
                            if(curRemoved.nodeType == 1 && curRemoved.hasAttribute("data-seq")){
                                me.removeOppositeTag(mutation.removedNodes[i]);
                            }
                            me.unselectQmSegment();
                        }
                    }
                });
            });
        }
        var observConfig = {
            childList: true,
            subtree: true
        };
        me.DOMObserver.observe(editor.getDoc().querySelector("body"), observConfig);
    },
    selectQmSegment: function(qmTag){
        var me = this,
            sev = me.getQmFieldset().down('combo[name="qmsubseverity"]'),
            commentField = me.getQmFieldset().down('textfield[name="qmsubcomment"]');

        me.selectedQmSegment = qmTag;

        sev.setValue(me.getSeverityFromQmSegment(me.selectedQmSegment));
        commentField.setValue(me.selectedQmSegment.getAttribute("data-comment"));
        //TODO This need to be method in the View
        me.getQMAddBtn().setText("Change selected segment QM type");
    },
    unselectQmSegment: function(){
      var me = this,
          sev = me.getQmFieldset().down('combo[name="qmsubseverity"]'),
          commentField = me.getQmFieldset().down('textfield[name="qmsubcomment"]');

        sev.reset();
        commentField.reset();
        //TODO This need to be method in the View
        me.getQMAddBtn().setText("Add QM");
        me.selectedQmSegment = null;
    },
    /**
     * displays the QM Summary Window
     */
    showQmSummary: function() {
        this.getWindow().show();
    },
    /**
     * Inserts the QM Issue Tag in the Editor by key shortcut, displays popup if nothing selected
     * @param key
     */
    handleAddQmFlagKey: function(key) {
        var me = this,
            found = false,
            menuitem = me.getQmFieldset().down('menuitem[qmid='+key+']');
        
        if (menuitem) {
            me.handleAddQmFlagClick(menuitem);
        }
    },
    handleQmFlagClick: function(menuitem){
        var me = this;
        if(me.selectedQmSegment !== null){
            me.handleModifyQmFlagClick(menuitem);
        } else {
            me.handleAddQmFlagClick(menuitem);
        }
    },
    /**
     * Inserts the QM Issue Tag in the Editor, displays popup if nothing selected
     * @param menuitem
     */
    handleAddQmFlagClick: function(menuitem) {
        var me = this,
            sev = me.getQmFieldset().down('combo[name="qmsubseverity"]'),
            commentField = me.getQmFieldset().down('textfield[name="qmsubcomment"]'),
            format = Ext.util.Format,
            comment = format.stripTags(commentField.getValue()).replace(/[<>"'&]/g,'');
            //@todo when we are going to make qm subsegments editable, we should improve the handling of html tags in comments.
            //since TRANSLATE-80 we are stripping the above chars, 
            //because IE did not display the segment content completly with qm subsegments containing these chars
            //WARNING: if we allow tags and special chars here, we must fix CSV export too! See comment in export/FileParser/Csv.php
                
        me.addQmFlagToEditor(menuitem.qmid, comment, sev.getValue());
        sev.reset();
        commentField.reset();
        me.addQmFlagHistory(menuitem);
    },
    getTagsArray: function(tag){
        var me = this,
            tagType = me.getTagType(tag),
            oppositeTagType = me.getOppositeTagType(tagType),
            tagsArray = {};
        tagsArray[tagType] = tag;
        tagsArray[oppositeTagType] = me.selectOppositeTag(tag);
        return tagsArray;
    },
    getTagType: function(tag){
        var match = /qm-image-(close|open)-/.exec(tag.id);
        return match[1];
    },
    getOppositeTagType: function(tagType){
        return tagType === "open"  ? "close" : "open";
    },
    selectOppositeTag: function(tag){
        var me = this,
            editor = me.getSegmentGrid().editingPlugin.editor.mainEditor,
            tagType = me.getTagType(tag);
        return editor.getDoc().querySelector("#" + tag.id.replace(tagType,me.getOppositeTagType(tagType)));
    },
    removeOppositeTag: function(tag){
        var me = this,
            oppositeTag = me.selectOppositeTag(tag),
            editor = me.getSegmentGrid().editingPlugin.editor.mainEditor;
        if(oppositeTag && oppositeTag.parentElement.contains(oppositeTag)){
            me.DOMObserver.disconnect();
            //oppositeTag.removeEventListener("DOMSubtreeModified", me.DOMNodeRemovedFunction);
            oppositeTag.parentElement.removeChild(oppositeTag);
            me.setDOMNodeRemovedEvent(editor);
        }
    },
    /**
     * Modify the currently selected QM Issue Tag in the Editor
     * @param menuitem
     */
    handleModifyQmFlagClick: function(menuitem) {

        var me = this;

        me._saveCurrentMQM(menuitem.qmid);

        //@todo when we are going to make qm subsegments editable, we should improve the handling of html tags in comments.
        //since TRANSLATE-80 we are stripping the above chars,
        //because IE did not display the segment content completly with qm subsegments containing these chars
        //WARNING: if we allow tags and special chars here, we must fix CSV export too! See comment in export/FileParser/Csv.php

        /*
        me.addQmFlagToEditor(menuitem.qmid, comment, sev.getValue());
        sev.reset();
        commentField.reset();
        me.addQmFlagHistory(menuitem);*/
    },
    getQmIdFromQmSegment: function(qmSegment){
        var regexp = /qmflag-([0-9]+)/;
        return qmSegment.className.match(regexp)[1];
    },
    getSeverityFromQmSegment: function(qmSegment){
        var regexp = /([^ ]+)/;
        return qmSegment.className.match(regexp)[1];
    },
    _saveCurrentMQM: function(qmid){
        var me = this,
            sev = me.getQmFieldset().down('combo[name="qmsubseverity"]');
            commentField = me.getQmFieldset().down('textfield[name="qmsubcomment"]'),
            format = Ext.util.Format,
            comment = format.stripTags(commentField.getValue()).replace(/[<>"'&]/g,''),
            tagDef = this.getImgTagDomConfig(qmid, comment, sev.getValue()),
            tagsToBeModified = me.getTagsArray(me.selectedQmSegment),
            impFromConfig = ["src", "data-comment"];

            for(var i in tagsToBeModified){
                var curTag = tagsToBeModified[i],
                    curConfig = tagDef[i];
                curTag.className = curConfig.cls;
                impFromConfig.forEach(function(key){
                    curTag.setAttribute(key, curConfig[key]);
                });
            }
    },
    saveCurrentMQM: function(){
        var me = this,
            qmid = me.getQmIdFromQmSegment(me.selectedQmSegment);
        me._saveCurrentMQM(qmid);
    },
    /**
     * Inserts the QM Issue IMG Tags around the text selection in the editor 
     * @param {Number} qmid Qm Issue ID
     * @param {String} comment 
     * @param {String} sev Severity ID
     * @return {Boolean}
     */
    addQmFlagToEditor: function(qmid, comment, sev){
        var me = this,
            editor = me.getSegmentGrid().editingPlugin.editor.mainEditor,
            tags;
        if(Ext.isIE) { //although >IE11 knows ranges we can't use it, because of an WrongDocumentError
            tags = me.insertQmFlagsIE(editor,qmid, comment, sev);
        } else {
            tags = me.insertQmFlagsH5(editor,qmid, comment, sev);
        }
        me.lastSelectedRangeIE = null;
        me.selectQmSegment(tags.open);
        return true;
    },
    /**
     * QM IMG Tag Inserter for for HTML5 Browsers (= not IE) 
     * @param {Editor.view.segments.HtmlEditor} editor
     * @param {Number} qmid Qm Issue ID
     * @param {String} comment 
     * @param {String} sev Severity ID
     */
    insertQmFlagsH5: function(editor, qmid, comment, sev){
		var doc = editor.getDoc(),
			rangeBegin = doc.getSelection().getRangeAt(0),
			rangeEnd = rangeBegin.cloneRange(),
			tagDef = this.getImgTagDomConfig(qmid, comment, sev),
			open = Ext.DomHelper.createDom(tagDef.open),
			close = Ext.DomHelper.createDom(tagDef.close);
		rangeBegin.collapse(true);
		rangeEnd.collapse(false);
		rangeEnd.insertNode(close);
		rangeBegin.insertNode(open);
		doc.getSelection().removeAllRanges();
		rangeEnd.collapse(false);
		doc.getSelection().addRange(rangeEnd);
        return {"open": open, "close": close};
    },
    /**
     * generates a Dom Config Object with the to image tags 
     * @param {Number} qmid Qm Issue ID
     * @param {String} comment 
     * @param {String} sev Severity ID
     * @returns {Object}
     */
    getImgTagDomConfig: function(qmid, comment, sev) {
    	var me = this, 
    		uniqid = Ext.id(),
    		config = function(open){
    		return {
    			tag: 'img', 
    			id: 'qm-image-'+(open ? 'open' : 'close')+'-'+uniqid, 
    			'data-comment': (comment ? comment : ""), 
    			'data-seq': uniqid, 
    			//minor qmflag qmflag-2 ownttip open
    			cls: sev+' qmflag ownttip '+(open ? 'open' : 'close')+' qmflag-'+qmid, 
    			src: me.getImgTagSrc(qmid, open)
    		};
    	};
    	return {
    		open: config(true),
    		close: config(false)
    	};
    },
    /**
     * generates the image tag src
     * @param {String} qmid
     * @param {Boolean} open
     * @returns {String}
     */
    getImgTagSrc: function(qmid, open) {
    	return Editor.data.segments.subSegment.tagPath+'qmsubsegment-'+qmid+'-'+(open ? 'left' : 'right')+'.png';
    },
    /**
     * maintains the last used QM Flags in the first level of the menu
     * @param {Ext.menu.Item} menuitem
     */
    addQmFlagHistory: function(menuitem) {
    	if(menuitem.parentMenu && !menuitem.parentMenu.parentMenu) {
    		return; //ignore first level and history menu entries
    	}
    	var me = this,
    	id = menuitem.qmid,
    	toremove,
    	toadd = Ext.applyIf({}, menuitem.initialConfig),
    	menu = me.getAddMenuFirstLevel();
    	if(! me.lastUsed){
        	me.lastUsed = [];
        	me.historyTopIndex = menu.items.length + 1;
        	menu.add('-');
    	}
    	delete toadd.menu;
    	
    	//if already in list ignore
    	if(Ext.Array.contains(me.lastUsed, id)){
    		Ext.Array.remove(me.lastUsed, id); // remove from stack
    		me.lastUsed.push(id); //and put it to the end
    		return;
    	}
    	
    	menu.insert(me.historyTopIndex, toadd);
    	
    	//cycle through lastused id array
    	me.lastUsed.push(id);
    	if(me.lastUsed.length > 5) {
    		toremove = me.lastUsed.shift();
    		toremove = menu.query('> menuitem[qmid="'+toremove+'"]');
    		if(toremove.length > 0){
    			toremove[0].destroy();
    		}
    	}
    },
    /**
     * QM IMG Tag Inserter for IE, which don't support ranges in older versions
     * @see http://stackoverflow.com/questions/1470932/ie8-iframe-designmode-loses-selection
     * @see http://www.webmasterworld.com/javascript/3820483.htm
     * 
     * for IE11 range / selection things were working, but we are getting issues about different documents on inserting the DomNode
     * so we keep on our "strong" replacing method for IE11
     * @see http://stackoverflow.com/questions/2284356/cant-appendchild-to-a-node-created-from-another-frame
     * 
     * @param {Editor.view.segments.HtmlEditor} editor
     * @param {Number} qmid Qm Issue ID
     * @param {String} comment
     * @param {String} sev
     */
    insertQmFlagsIE: function(editor, qmid, comment, sev){
    	var doc = editor.getDoc(),
    		sep = Editor.TRANSTILDE,
    		tagDef = this.getImgTagDomConfig(qmid, comment, sev),
    		open = Ext.DomHelper.createDom({tag: 'div', children:tagDef.open}),
    		close = Ext.DomHelper.createDom({tag: 'div', children:tagDef.close}),
    		sel = doc.getSelection ? doc.getSelection() : doc.selection,
    		newValue;
        if(! this.isEmptySelectionIE(doc.selection)){
        	doc.execCommand('bold', false); //fake selection (in case htmleditor was not deselected)
        }
        this.lastSelectedRangeIE = null;
    	newValue = doc.body.innerHTML.replace(/<(\/?)strong>/ig, sep);
    	newValue = newValue.split(sep);
    	Ext.Array.insert(newValue, 1, open.innerHTML);
    	Ext.Array.insert(newValue, -1, close.innerHTML);
    	doc.body.innerHTML = newValue.join('');
    	sel.removeAllRanges ? sel.removeAllRanges() : sel.empty();
        return {"open": open, "close": close};
    },
    /**
     * On Leaving the editor, IE looses the selection so save it by marking it bold and storing the range 
     * @param {Object} ev
     * @param {Node} iframeEl
     */
    handleEditorBlur: function(ev,iframeEl) {
    	var doc = iframeEl.contentWindow.document,
    		sel = doc.getSelection ? doc.getSelection() : doc.selection;
    	if(! this.isEmptySelectionIE(sel)){
    		return;
    	}
    	doc.execCommand('bold', false); //fake selection
    	Ext.fly(doc.body).addCls('fakedsel');
    	this.lastSelectedRangeIE = sel.createRange();
    },
    /**
     * On entering the editor in IE, restore the selection 
     * @param {Object} ev
     * @param {Node} iframeEl
     */
    handleEditorFocus: function(ev,iframeEl) {
    	if(this.lastSelectedRangeIE){
    		var doc = iframeEl.contentWindow.document; 
    		this.lastSelectedRangeIE.select();
    		Ext.fly(doc.body).removeCls('fakedsel'); //remove bold 
    		doc.execCommand('bold', false); //remove fake selection
    	}
    	this.lastSelectedRangeIE = null;
    },
	/**
	 * @param {Selection} sel
	 * @returns {Boolean}
	 */
    isEmptySelectionIE: function(sel){
    	return sel.type ? sel.type.toLowerCase() != 'none' : sel.isCollapsed;
    }
});
