/**
 * Created by jean on 6/6/2016.
 */


/*
 START LICENSE AND COPYRIGHT

 This file is part of translate5

 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.

 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

 END LICENSE AND COPYRIGHT
 *

/**#@++
 * @author Jean Duthon
 * @package editor
 * @version 1.0
 *
 */
/**
 * Keylogger controller is used to log the key and time during the postedit action.
 * @class Editor.controller.Keylogger
 * @extends Ext.app.Controller
 */
Ext.define('Editor.controller.Keylogger', {
    extend : 'Ext.app.Controller',
    stores: ['Segments'],
    require: ['Editor.controller.Segments'],
    models: ['Keylog'],
    isLogging: false,
    isInit: false,
    postEditField: false,
    refs:[{
        ref: 'segmentsHtmleditor',
        selector: 'segmentsHtmleditor'
    }],
    listen: {
        component: {
            'segmentsHtmleditor': {
                initialize: 'setKeylog'
            }
        },
        controller: {
            '#editorcontroller': {
                postEditingStart: 'resetController',
                postEditingStop: 'stopLog'
            },
            '#segmentscontroller': {
                saveComplete: 'persistLog'
            }
        }
    },

    eventsListener: [
        {eventId: 'keydown', fn: 'specialKeylog'},
        {eventId: 'keypress', fn: 'regularKeylog'},
        {eventId: 'blur',fn: 'removeKeylog', body: true}
    ],
    eventHandlers: {},
    loggingStartTime: 0,
    loggingEndTime: 0,
    keysLogged: [],


    specialModifiers: ["ctrlKey","metaKey","altKey"],
    otherModifiers: ["shiftKey"],

    init: function(){
        var me = this;
        me.allModifiers = this.specialModifiers.concat(this.otherModifiers);
    },

    getEventHandler: function(eventHandler){
        var me = this;
        var fn = me[eventHandler];
        if(!(eventHandler in me.eventHandlers)){
            me.eventHandlers[eventHandler] = fn.bind(me);
        }
        return me.eventHandlers[eventHandler];
    },

    getTimeDelta: function(time){
        var me = this;
        return time - me.loggingStartTime;
    },

    getCommonEventkeyProperties: function(ekpObject, e){
        var propertiesFromEvent = ["code","keyCode"],
            me = this;

        propertiesFromEvent.forEach(function(prop){
            ekpObject[prop] = e[prop];
        });
        ekpObject.position = me.getSegmentsHtmleditor().getWin().getSelection().anchorOffset;
        ekpObject.selection = me.getSegmentsHtmleditor().getWin().getSelection().toString();
        ekpObject.time = me.getTimeDelta(new Date());
        ekpObject.modifiers = me.allModifiers.filter(function(mod){
            return e[mod];
        });

        return ekpObject;

    },

    specialKeylog: function(e){
        var me = this,
            directions = ["Left", "Up", "Down", "Right"],
            keysBeg = ["Arrow", "Page"],
            directionKeys = [].concat.apply([],keysBeg.map(function(beg){ return directions.map(function(k){return beg + k;})})),
            keysToListen = directionKeys.concat(["Backspace"]),
            hasSpecialModifier = function(e){
                return me.specialModifiers.filter(function(sm){
                        return e[sm];
                    }).length > 0;
            },
            ekpObject = {};

        if(hasSpecialModifier(e)){
            ekpObject.type = 'action';
        } else if(keysToListen.indexOf(e.code) != -1){
            ekpObject.type = 'special';
        }
        //If one of those if/else was true, then we should log it
        if('type' in ekpObject){
            ekpObject = me.getCommonEventkeyProperties(ekpObject, e);
            me.keysLogged.push(ekpObject);
        }
    },

    regularKeylog: function(e){
        var ekpObject = {type: 'regular', key: String.fromCharCode(e.keyCode)},
            me = this;
        ekpObject = me.getCommonEventkeyProperties(ekpObject, e);
        me.keysLogged.push(ekpObject);
    },

    setKeylog: function(e){
        var me = this,
            htmlEditorDoc = me.getSegmentsHtmleditor().getDoc();

        if(me.isLogging){
            return;
        }
        me.eventsListener.forEach(function(eventHandler){
            me.addEventHandler(eventHandler);
        });
        if(!me.isInit){
            me.resetController();
        } else {
            console.log("Focus retrieved at " + me.getTimeDelta(new Date()));
            me.keysLogged.push({time: me.getTimeDelta(new Date()), type: 'focusActive'});
        }
        me.isLogging = true;
    },

    resetController: function(){
        var me = this;
        me.loggingStartTime = new Date();
        me.addEventHandler({eventId: 'focus', fn: 'setKeylog', body:true});
        me.isInit = true;
        console.log("Starting to log at " + me.loggingStartTime);
        me.postEditField = true;
        me.keysLogged = [];
    },

    removeEventHandler: function(eventHandlerStruct){
        this._changeEventHandler(eventHandlerStruct, false);
    },

    addEventHandler: function(eventHandlerStruct){
        this._changeEventHandler(eventHandlerStruct, true);
    },

    _changeEventHandler: function(eventHandlerStruct, add){
        var fnToCall,
            me = this,
            target = me.getSegmentsHtmleditor().getDoc();
        if(add){
            fnToCall = "addEventListener";
        } else {
            fnToCall = "removeEventListener";
        }
        if('body' in eventHandlerStruct){
            target = target.body;
        }
        target[fnToCall](eventHandlerStruct.eventId, me.getEventHandler(eventHandlerStruct.fn));
    },

    removeKeylog: function(e){
        var me = this;
        if(!me.isLogging){
            return;
        }
        me.eventsListener.forEach(function(eventHandler){
            me.removeEventHandler(eventHandler);
        });
        me.isLogging = false;
        me.keysLogged.push({time: me.getTimeDelta(new Date()), type: 'focusLost'});
        console.log("Focus lost at " + me.getTimeDelta(new Date()));
    },

    persistLog: function(record){
        var me = this,
                segmentId = record.get("id");
        
        if(!me.postEditField){
            return;
        } else {
            me.postEditField = false;
        }

        me.loggingEndTime = new Date();

        console.log({'segmentId': segmentId,
            'startTime': me.loggingStartTime,
            'endTime': me.loggingEndTime,
            'log': me.keysLogged});
        model = Ext.create('Editor.model.Keylog', {'segmentId': segmentId,
                                                    'startTime': me.loggingStartTime,
                                                    'endTime': me.loggingEndTime,
                                                    'log': me.keysLogged}
        );
        model.save({
            success: function(response){
                //console.log("success", response);
            },
            failure: function(response){
                console.log("failure", response);
            }
        });
    },

    stopLog: function(){
        var me = this;

        me.loggingEndTime = new Date();

        me.removeEventHandler({eventId: 'focus', fn: 'setKeylog', body:true});
        me.eventsListener.forEach(function(eventHandler){
            me.removeEventHandler(eventHandler);
        });
    }
});