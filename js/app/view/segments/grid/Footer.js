
/*
 START LICENSE AND COPYRIGHT

 This file is part of translate5

 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt
 included in the packaging of this file.  Please review the following information
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.

 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

 END LICENSE AND COPYRIGHT
 */

/**#@++
 * @author Jean Duthon
 * @package editor
 * @version 1.0
 *
 */
/**
 * @class Editor.view.segments.Grid.Footer
 * @extends Ext.toolbar.Toolbar
 */
Ext.define('Editor.view.segments.grid.Footer', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.segmentsFooter',

    itemId: 'segmentsFooter',

    //Item Strings:
    item_viewModesMenu: '#UT#Editormodi',
    item_viewModeBtn: '#UT#Ansichtsmodus',
    item_editModeBtn: '#UT#Bearbeitungsmodus',
    item_ergonomicModeBtn: '#UT#Ergonimic',
    item_hideTagBtn: '#UT#Tags verbergen',
    item_shortTagBtn: '#UT#Tag-Kurzansicht',
    item_fullTagBtn: '#UT#Tag-Vollansicht',
    item_qmsummaryBtn: '#UT#QM-Subsegment-Statistik',
    item_optionsTagBtn: '#UT#Einstellungen',
    item_clearSortAndFilterBtn: '#UT#Sortierung und Filter zurücksetzen',
    item_watchListFilterBtn: '#UT#Merkliste',

    MQMtotalString: 'Number of current MQM tags : ',
    TimerString: 'Key logging for postedit activated for ',
    timer: null,


    store: 'QmSummary',


    initConfig: function(instanceConfig) {
        var me = this,
            config = {
                items: [{
                    xtype: 'panel',
                    itemId: 'MQMtagsTotal'
                },{
                    xtype: 'tbseparator'
                },{
                    xtype: 'panel',
                    disabled: true,
                    itemId: 'posteditTimerPanel'
                }]
            };
        if (instanceConfig) {
            me.getConfigurator().merge(me, config, instanceConfig);
        }
        return me.callParent([config]);
    },

    updateMQMSummary: function(MQMtotal){
        var me = this;
        console.log(MQMtotal);
        me.getComponent('MQMtagsTotal').update(me.MQMtotalString + MQMtotal);
    },

    activateTimer: function() {
        var me = this,
            mins = 0,
            secs = 0,
            addFrontZeroIfNone = function(t){
                return t < 10 ? '0' + t : '' + t;
            };
        me.getComponent('posteditTimerPanel').enable();
        me.timer = window.setInterval(function(){
            secs += 1;
            if(secs == 60){
                mins += 1;
                secs = 0;
            }
            me.updateTimerPanel(addFrontZeroIfNone(mins) + ":" + addFrontZeroIfNone(secs));
        }, 1000);
    },
    updateTimerPanel: function(time){
        var me = this;
        me.getComponent('posteditTimerPanel').update(me.TimerString + time);
    },
    endTimer: function(){
        var me = this;
        window.clearInterval(me.timer);
        me.getComponent('posteditTimerPanel').disable();
        me.getComponent('posteditTimerPanel').update("");
    }
});