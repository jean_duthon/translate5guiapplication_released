
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Jean Duthon, Karyan Daze UG
 @author     Karyan Daze UG
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

Ext.define('Editor.view.segments.StateWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.segmentStateWindow',
    itemId: 'segmentStateWindow',
    cls: 'segmentStateWindow',
    //TODO TRANSLATION
    title: 'Please choose a ranking',
    strings: {
        addBtn: '#UT#Task hinzufügen',
    },
    height : 520,
    width : 550,
    modal : true,

    initConfig: function(instanceConfig) {
        var me = this,
            config = {
                title: me.title, //see EXT6UPD-9
                items: [{
                    xtype: 'form',
                    padding: 5,
                    ui: 'default-frame',
                    itemId: 'statesForm',
                    defaults: {
                        labelWidth: 200,
                        anchor: '100%'
                    },
                    items: [{
                        xtype: 'fieldset',
                        itemId: 'metaStates',
                        defaultType: 'radio',
                        hideable: Editor.data.segments.showStatus,
                        hidden: !Editor.data.segments.showStatus,
                        title: me.item_metaStates_title,
                        items: me.getStateFlags()
                    }
                    ]
                },{
                    xtype: 'panel',
                    header: false,
                    //TODO Translation
                    html: "You can also use keyboards digit shortcuts to select the state."
                }],
                defaultFocus: 'statesForm',
                dockedItems: [{
                    xtype: 'toolbar',
                    dock: 'bottom',
                    ui: 'footer',
                    layout: {
                        type: 'hbox',
                        pack: 'start'
                    },
                    items: [{
                        xtype: 'button',
                        iconCls: 'ico-task-add',
                        itemId: 'save-state-btn',
                        //TODO TRANSLATION
                        text: "Save status"
                    }]
                }]
            };

        if (instanceConfig) {
            me.getConfigurator().merge(me, config, instanceConfig);
        }
        return me.callParent([config]);
    },

    getStateFlags: function(){
        var counter = 1,
            me = this;
      return Editor.data.segments.stateFlags.map(function(item){
          var tooltip = "";
          /*if(counter < 10) {
              tooltip = Ext.String.format(me.item_metaStates_tooltip, counter++);
          }
          else {
              tooltip = me.item_metaStates_tooltip_nokey;
          }*/
          return {
              name: 'stateId',
              inputValue: item.id,
              boxLabel: '<span data-qtip="'+tooltip+'">'+item.label+'</span>'
          };
      });
    }
});