
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**
 * @class Editor.segments.EditorKeyMap
 * @extends Ext.util.KeyMap
 * 
 * Own KeyMap implementation which uses handlers without delegation to work in the iframe body of the HtmlEditor
 */
Ext.define('Editor.view.segments.EditorKeyMap', {
    extend: 'Ext.util.KeyMap',
    enable: function() {
        var me = this;

        me.isDigitTarget = false;
        
        if (!me.enabled) {
            me.target.on(me.eventName, me.handleTargetEvent, me, {capture: me.capture, priority: me.priority, delegated: false});
            me.enabled = true;
        }
    },

    setDigitPreparation: function(digitPrep){
        var me = this;
        me.isDigitTarget = digitPrep;
    },

    /**
     * interceptor to handle the DIGITs prepared by another key kombination 
     */
    handleTargetEvent: function(event) {
        event.isDigitPreparation = false;
        event.lastWasDigitPreparation = this.lastWasDigitPreparation || this.isDigitTarget;
        var res = this.callParent([event]);
        this.lastWasDigitPreparation = event.isDigitPreparation;
        return res;
    }
});