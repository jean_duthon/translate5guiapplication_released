
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**#@++
 * @author Marc Mittag
 * @package editor
 * @version 1.0
 *
 */
/**
 * @class Editor.view.qmsubsegments.AddFlagFieldset
 * @extends Ext.form.FieldSet
 */
Ext.define('Editor.view.qmsubsegments.AddFlagFieldset', {
	extend : 'Ext.form.FieldSet',
	alias : 'widget.qmSubsegmentsFlagFieldset',
	title : "#UT#QM Subsegmente",
	collapsible: true,
	strings: {
		severityLabel: '#UT#Gewichtung',
		commentLabel: '#UT#Kommentar',
		qmAddBtn: '#UT#QM Subsegment hinzufügen'
	},
	initConfig: function(instanceConfig) {
		var me = this,
		    config = {
		        title: me.title, //see EXT6UPD-9
			items : [{
				xtype: 'button',
				id: 'QMAddBtn',
				text: me.strings.qmAddBtn,
				//margin: '0 0 0 0',
				menu: {
				    xtype: 'menu',
					bodyCls: 'qmflag-menu',
					items: instanceConfig.menuConfig,
					listeners: {
	                    afterrender: function(component) {
	                    	if(component.keyNav) {
	                    		component.keyNav.disable();
	                    	}
	                    }
                	}
				},
				//TODO Translation
				tooltip: "To add a new QM please select text and choose the issue type here. <br/>" +
				         "To delete a tooltip remove it from the text <br/>" +
						"To modify one click on it and edit it."
			},{
				xtype: 'combo',
				anchor: '100%',
				name: 'qmsubseverity',
				queryMode: 'local',
				autoSelect: true,
				fieldLabel: me.strings.severityLabel,
				forceSelection: true,
				editable: false,
			    displayField: 'text',
			    valueField: 'id'
			},{
				xtype: 'textfield',
				anchor: '100%',
				fieldLabel: me.strings.commentLabel,
				name: 'qmsubcomment'
			},{
				xtype: 'button',
				id: 'QMSaveBtn',
				//TODO TRANSLATION
				text: "Save"
			}]
		};
        if (instanceConfig) {
            me.getConfigurator().merge(me, config, instanceConfig);
        }
        return me.callParent([config]);
	}
});