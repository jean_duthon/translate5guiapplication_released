
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**
 * @class Editor.model.admin.User
 * @extends Ext.data.Model
 */
Ext.define('Editor.model.admin.User', {
  extend: 'Ext.data.Model',
  fields: [
    {name: 'id', type: 'int'},
    {name: 'userGuid', type: 'string'},
    {name: 'firstName', type: 'string'},
    {name: 'surName', type: 'string'},
    {name: 'gender', type: 'string'},
    {name: 'login', type: 'string'},
    {name: 'longUserName', type: 'string', persist: false, convert: function(v, rec) {
        return Editor.model.admin.User.getLongUserName(rec);
    }},
    {name: 'email', type: 'string'},
    {name: 'roles', type: 'string'},
    {name: 'passwd', type: 'string'},
    {name: 'editable', type: 'boolean', persist: false}
  ],
  idProperty: 'id',
  proxy : {
    type : 'rest',
    url: Editor.data.restpath+'user',
    reader : {
        rootProperty: 'rows',
        type : 'json'
    },
    writer: {
      encode: true,
      rootProperty: 'data',
      writeAllFields: false
    }
  },
  statics: {
      getUserName: function(rec) {
        return rec.get('firstName')+' '+rec.get('surName');
      },
      getLongUserName: function(rec) {
          return rec.get('surName')+', '+rec.get('firstName')+' ('+rec.get('login')+')';
      }
  },
  getUserName: function() {
      return this.self.getUserName(this);
  },
  isAllowed: function(right, task) {
      var me = this,
          isAllowed = (Ext.Array.indexOf(Editor.data.app.userRights, right) >= 0);
      if(!task) {
          return isAllowed;
      }
      var notOpenable = ! task.isOpenable();
      switch(right) {
          case 'editorReopenTask':
              if(!task.isEnded()) {
                  return false;
              }
              break;
          case 'editorEndTask':
              if(task.isEnded() || task.isLocked()) {
                  return false;
              }
              break;
          case 'editorOpenTask':
              if(notOpenable) {
                  return false;
              }
              break;
          case 'editorEditTask':
              if(notOpenable || task.isReadOnly()) {
                  return false;
              }
              break;
          case 'editorFinishTask':
              //TODO role visitor should be encapsulated some how
              //if user is not associated to the task or task is already finished, it cant be finished
              //TODO this is a good example how currently workflow state transtions 
              //     are encapuslated in JS, we want to finish the task, and have to check the current state.
              //     This should also come from the PHP workflow definition.
              if(task.get('userRole') == 'visitor' || task.get('userRole') == '' || task.isWaiting() || task.isFinished() || task.isEnded()) {
                  return false;
              }
              break;
          case 'editorUnfinishTask':
              //if user is not associated to the task or task is not finished, it cant be unfinished
              if(task.get('userRole') == '' || !task.isFinished() || task.isEnded()) {
                  return false;
              }
              break;
          case 'editorShowexportmenuTask':
              if(!task.hasQmSub() && !me.isAllowed('editorExportTask')){
                  return false;
              }
              break;
          case 'useChangeAlikes':
              if(!task.get('defaultSegmentLayout')) {
                  return false;
              }
              break;
      }
      // @todo should we move the rights into the model?
      return isAllowed;
  }
});